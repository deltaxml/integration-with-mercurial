#! /bin/bash

SCRIPT_DIR="`dirname ${BASH_SOURCE[0]}`";

file1=$1
file2=$2
file3=$3

i=1;
while [ ! -z "${@: 4+$i:1}" ]
do
     if [ -n "$params" ]
     then
       params+=" "
     fi   
     params+=${@: 4+$i:1}
     i=`expr $i + 1`
done

if [ -n "$params" ]
then  
  java -jar ${SCRIPT_DIR}/../../deltaxml-merge.jar merge concurrent3 ancestor $file1 edit1 $file2 edit2 $file3 $4 $params
else
  java -jar ${SCRIPT_DIR}/../../deltaxml-merge.jar merge concurrent3 ancestor $file1 edit1 $file2 edit2 $file3 $4
fi

if [ ! -f ${SCRIPT_DIR}/MercurialCheckConflict.class ]; then
  javac ${SCRIPT_DIR}/MercurialCheckConflict.java
fi
java -cp ${SCRIPT_DIR} MercurialCheckConflict $4
exit $?